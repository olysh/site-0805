// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome dev`

module.exports = {
  siteName: 'Gridsome',
  siteDescription: 'A simple, hackable & minimalistic starter for Gridsome that uses Netlify CMS for content.',

  transformers: {
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        '@gridsome/remark-prismjs'
      ]
    }
  },

  plugins: [
    // Posts markdown
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Post',
        path: 'content/posts/*.md',
        refs: {
          tags: {
            typeName: 'Tag',
            create: true
          }
        }
      }
    },
    // Features markdown
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Feature',
        path: 'content/features/*.md',
      }
    },
    // Blog markdown
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Blog',
        path: 'content/blogs/*.md',
      }
    },

    {
      use: `gridsome-plugin-netlify-cms`,
      options: {
        publicPath: `/admin`,
        modulePath: `src/admin/index.js`
      }
    },
  ],

  templates: {
    Post: '/:title',
    Tag: '/tag/:id'
  },
}
