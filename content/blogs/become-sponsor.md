---
title: How to become a sponsor?
cover_image: ../../static/images/uploads/blog_1.jpg
description: >-
  Markdown is intended to be as easy-to-read and easy-to-write as is feasible.
  Readability, however, is emphasized above all else. A Markdown-formatted
  document should be publishable as-is, as plain text, without looking like it's
  been marked up with tags or formatting instructions.
---
